package com.example.friday;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    GameEngine pongGame;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate( savedInstanceState);

        //Get the display size
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        //Initialize GameEngine object
        //PAss the screen size

        pongGame = new GameEngine(this,size.x,size.y);

        // make game engine view of activity
        setContentView(pongGame);


    }


    //Android lifecycle functions
    @Override
    protected void onPause(){
        super.onPause();
        //pausse th game
        pongGame.pauseGame();
    }




    @Override
    protected void onResume(){
        super.onResume();

        // start the game
        pongGame.startGame();


    }



}
