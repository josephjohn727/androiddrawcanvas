package com.example.friday;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
//import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import android.view.SurfaceView;

public class GameEngine extends SurfaceView implements Runnable {

    //screen and drawing setup variables

    //Debug variable
    final static String TAG = "PONG-GAME";

    //screen and drawing setup variables
    //screen size
    int screenWidth;
    int screenHeight;

    //game state
    boolean gameIsRunning;

    //threading
    Thread gameThread;

    //Drawing variables

    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;


    // Game specific variables

    // sprites
    int ballXPosition;
    int ballYposition;


    // game stats - number of lives scores etc


    public GameEngine(Context context, int w, int h){
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();


        this.ballXPosition = screenWidth/2;
        this.ballYposition = screenHeight/2;



    }

    // Helper functions
    private   void printScreenInfo(){
        Log.d(TAG, "Screen (w,h) = " + this.screenWidth + "," + this.screenHeight);
    }




    //GAme state functions(run , stop, start)
    @Override
    public void run() {
        while (gameIsRunning == true){
//            this.updatePosition;
//            this.redrawSprites;
//            this.setFPS();

        }
    }

    public void pauseGame(){
        gameIsRunning = false;
        try{
            gameThread.join();
        } catch (InterruptedException e){
            //Error
        }

    }

    public void startGame(){
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();

    }

    // Game engine functions
        // UPDATE DRAW SETFPS
    public void updatePosition(){
        this.ballXPosition = this.ballXPosition + 10;
    }


    // user input functions

}
